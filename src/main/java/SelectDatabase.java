
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADMIN
 */
public class SelectDatabase {
    public static void main(String[] args) {
         Connection conn = null;
            String url = "jdbc:sqlite:D-Coffee.db";
        try {        
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        } 
            String sql = "SELECT * FROM Category";
            
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                System.out.println(rs.getInt("Category_ID") + " " + rs.getString("Category_Name"));
            }
        } catch (SQLException ex) {
           System.out.println(ex.getMessage());
            return;
        }
        
        
        
        
        
        
        
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        
    }
}
