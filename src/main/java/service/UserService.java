/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import dao.UserDao;
import model.User;

/**
 *
 * @author ADMIN
 */
public class UserService {
    public User Login(String name, String password){
        UserDao userDao = new UserDao();
        User user =userDao.getByName(name);
        if(user != null && user.getPassword().equals(password)){
            return user;
        }
        return null;
    }
}
