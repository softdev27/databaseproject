/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 *
 * @author ADMIN
 */
public class UserDao implements Dao<User>{

    @Override
    public User get(int id) {
        User user = null;
         String sql = "SELECT * FROM User WHERE User_ID=?";
         Connection conn = DatabaseHelper.getConnect();
            
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
                user = new User();
                user.setId(rs.getInt("User_ID"));
                user.setName(rs.getString("User_Name"));
                user.setRole(rs.getInt("User_Role"));
                user.setGender(rs.getString("User_Gender"));
                user.setPassword(rs.getString("User_Password"));
                
            }
        } catch (SQLException ex) {
           System.out.println(ex.getMessage());
            
        }
        return user;
    }
    

    public User getByName(String name) {
        User user = null;
         String sql = "SELECT * FROM User WHERE User_Name=?";
         Connection conn = DatabaseHelper.getConnect();
            
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
                user = new User();
                user.setId(rs.getInt("User_ID"));
                user.setName(rs.getString("User_Name"));
                user.setRole(rs.getInt("User_Role"));
                user.setGender(rs.getString("User_Gender"));
                user.setPassword(rs.getString("User_Password"));
                
            }
        } catch (SQLException ex) {
           System.out.println(ex.getMessage());
            
        }
        return user;
    }

   
    public List<User> getAll() {
        ArrayList<User> list = new ArrayList();
         String sql = "SELECT * FROM User";
         Connection conn = DatabaseHelper.getConnect();
            
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                User user = new User();
                user.setId(rs.getInt("User_ID"));
                user.setName(rs.getString("User_Name"));
                user.setRole(rs.getInt("User_Role"));
                user.setGender(rs.getString("User_Gender"));
                user.setPassword(rs.getString("User_Password"));
                list.add(user);
            }
        } catch (SQLException ex) {
           System.out.println(ex.getMessage());
            
        }
        return list;
    }
    @Override
      public List<User> getAll(String where, String order) {
        ArrayList<User> list = new ArrayList();
         String sql = "SELECT * FROM  User  where " +where+"  ORDER  BY  "+ order ;
         Connection conn = DatabaseHelper.getConnect();
            
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                User user = new User();
                user.setId(rs.getInt("User_ID"));
                user.setName(rs.getString("User_Name"));
                user.setRole(rs.getInt("User_Role"));
                user.setGender(rs.getString("User_Gender"));
                user.setPassword(rs.getString("User_Password"));
                list.add(user);
            }
        } catch (SQLException ex) {
           System.out.println(ex.getMessage());
            
        }
        return list;
    }

    @Override
    public User save(User obj) {
         String sql = "INSERT  INTO  User  (User_Name, User_Gender, User_Password, User_Role)  "
                 +"VALUES (? ,? ,? ,?)";
         Connection conn = DatabaseHelper.getConnect();
            
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getGender());
            stmt.setString(3, obj.getPassword());
            stmt.setInt(4, obj.getRole());
   //         System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedID(stmt);
           obj.setId(id);
            
        } catch (SQLException ex) {
           System.out.println(ex.getMessage());
           return null;
            
        }
        return obj;
    }

    @Override
    public User update(User obj) {
             User user = null;
         String sql = "UPDATE User "
                 + "SET User_Name=?,User_Gender=?, User_Password=?,User_Role=? "
                 + "WHERE User_ID=? " ;
         Connection conn = DatabaseHelper.getConnect();
            
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getGender());
            stmt.setString(3, obj.getPassword());
            stmt.setInt(4, obj.getRole());
            stmt.setInt(5, obj.getId());
   //         System.out.println(stmt);
           int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
            
        } catch (SQLException ex) {
           System.out.println(ex.getMessage());
           return null;
            
        }
    }

    @Override
    public int delete(User obj) {
         String sql = "DELETE  FROM User WHERE User_ID=?";
         Connection conn = DatabaseHelper.getConnect();
            
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret =stmt.executeUpdate();
           return ret;
            
        } catch (SQLException ex) {
           System.out.println(ex.getMessage());
            
        }
      return -1;
    }
    
}
